package com.example.superstar.news_test;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsDetails extends AppCompatActivity {
    @BindView(R.id.text_view_text_news_details) TextView textViewText;
    @BindView(R.id.text_view_date_time_news_details) TextView textViewDateTime;
    @BindView(R.id.image_view_photo_news_details) ImageView imageView;
    @BindView(R.id.text_view_heading_news_details) TextView textViewHeading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ButterKnife.bind(NewsDetails.this);
        Intent intent = getIntent();
        int Image = intent.getIntExtra("Image",R.drawable.a1);
        String Date = intent.getStringExtra("Date");
        String Time = intent.getStringExtra("Time");
        String Heading = intent.getStringExtra("Heading");
        String Text = intent.getStringExtra("Text");

        textViewDateTime.setText(Date + " " + Time);
        imageView.setImageResource(Image);
        textViewHeading.setText(Heading);
        textViewText.setText(Text);



    }
}

package com.example.superstar.news_test;

/**
 * @author SuperStar
 * @since 4/22/2018
 */

public class News {
    private int image;
    private String heading;
    private String text;
    private String date;
    private String time;

    public News(int image, String heading, String text, String date, String time) {
        this.image = image;
        this.heading = heading;
        this.text = text;
        this.date = date;
        this.time = time;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

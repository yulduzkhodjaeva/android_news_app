package com.example.superstar.news_test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view_news) RecyclerView RecyclerViewNews;
    NewsAdapter newsAdapter;


    private List <News> newsListUzbek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

            News news1 = new News(R.drawable.a2, "Xalq dardini joyladim dilga", "Ushbu to'rtlik muallifi 1938 yili mana shu satrlarni oqqa tushirgan bo'lsa, oradan 11 yil o'tib, aynan shu so'zlari uchun sovuq Sibir o'lkasiga surgun qilindi.\n" +
                    "\n" +
                    "25 yil qamoq jazosiga mahkum etilgan shoir haqiqatning yuzini yashirmadi, taqdiriga tan berib, uzoq o'lkalarda rizq-nasibasini topib kun kechirdi. Bu inson 96 yoshni qarshilagan O'zbekiston Respublikasi xalq shoiri Shukrullodir.", "April 23, 2018", "21:39");
            News news2 = new News(R.drawable.a3, "Shavkat Mirziyoyevning xalqaro tashriflari: sarhisob va keyingi davlatlar", "O'zbekiston Respublikasi Prezidenti Shavkat Mirziyoyev o'tgan yil davomida 9ta davlatga, xususan 16 marta turli darajadagi tashriflarni amalga oshirdi. O'zbekiston yetakchisining ilk rasmiy tashrifi bundan 13 oy avval, Turkmanistonga bo'lgandi.\n" +
                    "\n" +
                    "Shundan so'ng, Ostona va Moskvaga rasmiy tashrifni amalga oshirgan Shavkat Mirziyoyev 2017 yilda Xitoy, Saudiya Arabistoni va AQShda ham bo'ldi.", "April 24, 2018", "21:00");

            News news3 = new News(R.drawable.a4, "O'zbekiston jahon fuqarolik indeksi ro'yxatida 117-o'rinni egalladi", "2017 yil yakunlariga ko'ra, eng qimmatli fuqarolikka ega mamlakatlar ro'yxatida birinchi o'rinni 81,7 foiz natija bilan Fransiya egallagan. O'zbekiston ushbu ro'yxatda 27,2 foiz natija bilan 117-o'rinda turibdi, deb yozadi henleyglobal.com.", "April 22, 2018", "17:00");
            News news4 = new News(R.drawable.a5, "Ilgizar Sobirov Xorazm viloyati hokimligidan ketdi, uning o'rniga yangi rahbar tayinlandi", "21 aprel kuni Xorazm viloyati xalq deputatlari kengashining navbatdan tashqari yig'ilishi bo'lib o'tdi. O'zbekiston bosh vaziri Abdulla Aripov ishtirokida kechgan yig'ilishda tashkiliy masala ko'rilgan.\n" +
                    "\n" +
                    "«Kun.uz» manbasining tasdiqlashicha, 2017 yilning 15 oktabrida IIV rahbarligiga o'tgan Po'lat Bobojonov o'rniga viloyatga hokim bo'lgan Ilgizar Matyoqubovich Sobirov o'z arizasiga ko'ra lavozimidan ozod etilgan.", "April 22, 2018", "11:10");
            News news5 = new News(R.drawable.a6, "Shahram G'iyosov va Murodjon Ahmadaliyev g'alabali yurishni davom ettirishdi", "Boks bo'yicha O'zbekiston terma jamoasi va «Uzbek Tigers»ning sobiq a`zolari Murodjon Ahmadaliyev va Shahram G'iyosov professional ringdagi navbatdagi janglarini Nyu-Yorkda o'tkazishdi.\n" +
                    "\n" +
                    "Bruklindagi Kings Theatre arenasidagi boks oqshomida dastlab ringga Rio-2016 Olimpiadasi bronza medali sohibi Murodjon Ahmadaliyev ko'tarildi. Uning raqibi argentinalik Karlos Kaston Suares bo'ldi. Bu bokschi shu vaqtgacha professional ringda 7 jang o'tkazib, 4 g'alaba (2ta nokaut) va 3 mag'lubiyat (1 ta nokaut) qayd etgandi.", "April 22, 2018", "09:10");

            newsListUzbek = new ArrayList<>();
            newsListUzbek.add(news1);
            newsListUzbek.add(news2);
            newsListUzbek.add(news3);
            newsListUzbek.add(news4);
            newsListUzbek.add(news5);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this,LinearLayoutManager.VERTICAL,false);
            RecyclerViewNews.setLayoutManager(linearLayoutManager);
            newsAdapter = new NewsAdapter(newsListUzbek,R.layout.list_item);
            newsAdapter.setOnItemClickListener(new NewsAdapter.onItemClickListener() {
                @Override
                public void onItemClickListener(View view, int position) {

                    Intent intent =  new Intent(MainActivity.this, NewsDetails.class);
                    intent.putExtra("Image",newsListUzbek.get(position).getImage());
                    intent.putExtra("Heading",newsListUzbek.get(position).getHeading());
                    intent.putExtra("Text",newsListUzbek.get(position).getText());
                    intent.putExtra("Date", newsListUzbek.get(position).getDate());
                    intent.putExtra("Time",newsListUzbek.get(position).getTime());
                    startActivity(intent);
                }
            });
            RecyclerViewNews.setAdapter(newsAdapter);








    }
}

package com.example.superstar.news_test;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author SuperStar
 * @since 4/22/2018
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder>{
    private final List<News> newsList;
    private final int rowLayout;


    private onItemClickListener mItemClickListener;
    public NewsAdapter(List<News> newsList, @LayoutRes int rowLayout) {
        this.newsList = newsList;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        News currentNews = newsList.get(position);
        String dateAndTime = currentNews.getDate() + "  " + currentNews.getTime();
        holder.textViewDateTime.setText(dateAndTime);
        holder.imageViewPhoto.setBackgroundResource(currentNews.getImage());
        holder.textViewHeading.setText(currentNews.getHeading());

    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener   {

        @BindView(R.id.text_view_date_time) TextView textViewDateTime;
        @BindView(R.id.image_view_photo) ImageView imageViewPhoto;
        @BindView(R.id.text_view_heading) TextView textViewHeading;

        private ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view){
            if(mItemClickListener!= null){
                mItemClickListener.onItemClickListener(view,getAdapterPosition());
            }
        }
    }

    public void setOnItemClickListener(onItemClickListener mItemClickListener){
        this.mItemClickListener = mItemClickListener;
    }
    public interface onItemClickListener{
        void onItemClickListener(View view, int position);
    }
}
